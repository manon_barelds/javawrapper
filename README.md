# README #


### What is this repository for? ###

* Repository which contains a java project to classify the HBA1C level in the blood. Makes use of a weka model.
* Version 2020

### How do I get set up? ###

* Clone repository and open it in Intellij
* Use the java-jaar3-1.0.0-all.jar to run the project in the commandline;
If you want to input an file : ##########Does not work yet
java -jar java-jaar3-1.0.0-all.jar -t file -f <Path to file>

If you want to input 1 instance
java -jar java-jaar3-1.0.0-all.jar -t instance -r <race> -g <gender> -a <age> -at <admission_type_id> -dd <discharge_disposition_id> -as <admission_source_id> -th <time_in_hospital> 
-nlp <num_lab_procedures> -np <num_procedures> -nm <num_medications> -no <number_outpatient> -ne <number_emergency> -ni <number_inpatient> -d1 <diag_1> -d2 <diag_2> -d3 <diag_3> 
-nd <number_diagnoses> -i <insulin>  -c <change> -dm <diabetesMed> -rm <readmitted>

You can copy the next line and it will work, or you can change the values in the values of your choice.
-r Caucasian -g Male -a [10-20) -at Emergeny -dd "Still in hospital" -as "Emergeny Room" -th 10 -nlp 3 -np 2 -nm 7 -no 7 -ne 3 -ni 5 -d1 "diseases of the circulatory system" -d2 "mental disorders" -d3 "injury and poisoning" -nd 5 -i Up  -c Ch -dm No -rm No

The instance will be printed with the HBA1C result at the last position, value Normal or Too high.
Normal says the HBA1C result is fine and Too high says the level is too high.
### Contribution guidelines ###

* Only the WekkaRunner is working with a file. You can change the file to your own file with the instances of the attribute A1Cresult as empty values. 
* You can also use your own model file, if you change the J48Manon.model to your own .model file (created in weka).
* The jar file does not work with a file jet but only with a lose instance, inputted in the command line.

### Who do I talk to? ###

* Manon Barelds
* m.barelds@st.hanze.nl

see https://bitbucket.org/manon_barelds/thema9/src/master/ for more information about the background 