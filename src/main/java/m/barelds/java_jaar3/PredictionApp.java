/*
 * Copyright (c) 2020 Manon Barelds
 * All rights reserved
 */
package m.barelds.java_jaar3;
/**
 * Main class
 *
 */
public class PredictionApp {

    /**
     * @param args The command line arguments
     */
    public static void main(String[] args) {
        OptionProvider processor = new OptionProvider(args);
        processor.start();
    }
}


