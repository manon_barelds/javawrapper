/*
 * Copyright (c) 2020 Manon Barelds
 * All rights reserved
 */
package m.barelds.java_jaar3;

import org.apache.commons.cli.*;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class OptionProvider {
    private static final String HELP = "help";
    private static final String TYPE = "type";
    private static final String FILE = "file";

    private static final String RACE = "race";
    private static final String GENDER = "gender";
    private static final String AGE = "age";
    private static final String ADMISSION_TYPE_ID = "admission_type_id";
    private static final String DISCHARGE_DISPOSITION_ID = "discharge_disposition_id";
    private static final String ADMISSION_SOURCE_ID = "admission_source_id";
    private static final String TIME_IN_HOSPITAL = "time_in_hospital";
    private static final String NUM_LAB_PROCEDURES = "num_lab_procedures";
    private static final String NUM_PROCEDURES = "num_procedures";
    private static final String NUM_MEDICATION = "num_medication";
    private static final String NUMBER_OUTPATIENT = "number_outpatient";
    private static final String NUMBER_EMERGENCY = "number_emergency";
    private static final String NUMBER_INPATIENT = "number_inpatient";
    private static final String DIAG_1 = "diag_1";
    private static final String DIAG_2 = "diag_2";
    private static final String DIAG_3 = "diag_3";
    private static final String NUMBER_DIAGNOSES = "number_diagnoses";
    private static final String INSULIN = "insulin";
    private static final String CHANGE = "change";
    private static final String DIABETESMED = "diabetesMed";
    private static final String READMITTED = "readmitted";

    private final String[] arguments;
    private Options options;
    private CommandLine cmdline;


    public OptionProvider(final String[] args) {
        this.arguments = args;
    }


    /**
     * Public start function calls all the important methods.
     */
    public void start() {
        createOptions();
        executeCommandLine();
        Instances unclassified = getInstances();
        Instances classified = classifyInstances(unclassified);
        printInstances(classified);
    }

    /**
     * creates the Options object.
     */
    private void createOptions() {
        // create Options
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Presents the help function to the user");
        Option typeOption = new Option("t", TYPE, true, "Input type; "
                + "To insert a single instance choose 'instance' "
                + "To insert a file (.csv or .arff), choose 'file' ");
        Option fileOption = new Option("f", FILE, true, "Please submit the path_to_file");
        Option raceOption = new Option("r", RACE, true, "The race of the patient (e.g. Caucasian)" +
                "Please insert \" around input when containing spaces.");
        Option genderOption = new Option("g", GENDER, true, "The gender of the patient " +
                "(Male/Female/X)");
        Option ageOption = new Option("a", AGE, true, "The age of the patient in intervals of " +
                "10 years (e.g. [0-10), [10-20), ..., [90,100)");
        Option admission_type_idOption = new Option("at", ADMISSION_TYPE_ID, true,
                "Type of admission of patient (e.g. Emergency,Missing,Elective,Urgent,Newborn," +
                        "Trauma Center) Please insert \" around input when containing spaces.");
        Option discharge_disposition_idOption = new Option("dd", DISCHARGE_DISPOSITION_ID, true,
                "Discharge disposition of patient (e.g. Discharged to home)" +
                        "Please insert \" around input when containing spaces.");
        Option admission_source_idOption = new Option("as", ADMISSION_SOURCE_ID, true,
                "Source of admission of patient (e.g. Emergency Room,Physician Referral" +
                        "Please insert \" around input when containing spaces.");
        Option time_in_hospitalOption = new Option("th", TIME_IN_HOSPITAL, true,
                "patient time in hospital in days (e.g. 1, 6 ");
        Option num_lab_proceduresOption = new Option("nlp", NUM_LAB_PROCEDURES, true, "number of "+
                "labprocedures (numeric)");
        Option num_proceduresOption = new Option("np", NUM_PROCEDURES, true, "Number of " +
                "procedures (numeric)");
        Option num_medicationOption = new Option("nm", NUM_MEDICATION, true, "Number of " +
                "medications (numeric)");
        Option number_outpatientOption = new Option("no", NUMBER_OUTPATIENT, true, "Number of " +
                "outpatients (numeric)");
        Option number_emergencyOption = new Option("ne", NUMBER_EMERGENCY, true, "Number of " +
                "emergencies (numeric)");
        Option number_inpatientOption = new Option("ni", NUMBER_INPATIENT, true, "Number of " +
                "inpatients (numeric)");
        Option diag_1Option = new Option("d1", DIAG_1, true, "Diagnoses 1 " +
                "(e.g 'diseases of the circulatory system','diseases of the genitourinary system') " +
                "Please insert \" around input when containing spaces");
        Option diag_2Option = new Option("d2", DIAG_2, true, "Diagnoses 2 " +
                "(e.g 'diseases of the circulatory system','diseases of the genitourinary system') " +
                "Please insert \" around input when containing spaces");
        Option diag_3Option = new Option("d3", DIAG_3, true, "Diagnoses 3 " +
                "(e.g 'diseases of the circulatory system','diseases of the genitourinary system') " +
                "Please insert \" around input when containing spaces");
        Option number_diagnosesOption = new Option("nd", NUMBER_DIAGNOSES, true, "Number of " +
                "diagnoses (numeric)");
        Option insulinOption = new Option("i", INSULIN, true, "Insulin (e.g No,Up,Steady,Down)");
        Option changeOption = new Option("c", CHANGE, true, "Change in medication (e.g Ch,No)");
        Option diabetesMedOption = new Option("dm", DIABETESMED, true, "Diabetic medication " +
                "(e.g Yes, No)");
        Option readmittedOption = new Option("rm", READMITTED, true, "Is the patient readmitted" +
                "(e.g NO,>30,<30)");

        options.addOption(helpOption);
        options.addOption(typeOption);
        options.addOption(fileOption);
        options.addOption(raceOption);
        options.addOption(genderOption);
        options.addOption(ageOption);
        options.addOption(admission_type_idOption);
        options.addOption(discharge_disposition_idOption);
        options.addOption(admission_source_idOption);
        options.addOption(time_in_hospitalOption);
        options.addOption(num_lab_proceduresOption);
        options.addOption(num_proceduresOption);
        options.addOption(num_medicationOption);
        options.addOption(number_outpatientOption);
        options.addOption(number_emergencyOption);
        options.addOption(number_inpatientOption);
        options.addOption(diag_1Option);
        options.addOption(diag_2Option);
        options.addOption(diag_3Option);
        options.addOption(number_diagnosesOption);
        options.addOption(insulinOption);
        options.addOption(changeOption);
        options.addOption(diabetesMedOption);
        options.addOption(readmittedOption);
    }

    private void executeCommandLine() {
        CommandLineParser parser = new DefaultParser();
        HelpFormatter hf = new HelpFormatter();
        try {
            this.cmdline = parser.parse(this.options, this.arguments);
            if (cmdline.hasOption(HELP)) {
                hf.printHelp("HBA1C level predictor", this.options, true);
                System.exit(0);
            }
        } catch (ParseException e) {
            hf.printHelp("HBA1C level predictor", this.options, true);
        }
    }

    /**
     * Sees if the user input is a file or an instance
     * File goes to FiletoInstance() and instances goes to InstancetoInstance
     * @return The unclassified instances from either a CSV file or Commandline input
     */
    private Instances getInstances() {
        if (cmdline.hasOption(TYPE)) {
            String type = cmdline.getOptionValue(TYPE).trim();
            type = type.toLowerCase();
            switch (type) {
                case "instance":
                    return InstancetoInstance();
                case "file":
                    return FiletoInstances();
                default:
                    throw new IllegalArgumentException("Type: " + type + " Is not correct, please use" +
                            "'instance' or 'file' and check --help");

            }
        } else {
            System.err.println("No Type option was submitted, Please use --help to see the parameters");
            System.exit(0);
            return null;
        }
    }

    /**
     * Gets the instances from the inputted file
     *
     *
     * @return The unclassified instances from file
     */
    private Instances FiletoInstances() {
        if (cmdline.hasOption(FILE)) {
            String filePath = cmdline.getOptionValue(FILE);

            try {
                final InputStream file = new FileInputStream(filePath);
                CSVLoader csvLoader = new CSVLoader();
                csvLoader.setSource(file);
                Instances instances = csvLoader.getDataSet();
                instances = setClass(instances);
                //System.out.println(instances);
                return instances;
            } catch (IOException e) {
                System.err.println("File: " + filePath + " Not found or unable to load, please try again!");
                System.exit(0);
                return null;
            }

        } else {
            System.err.println("No file was submitted");
            System.exit(0);
            return null;
        }
    }

    /**
     * Creates instance from input if all input options are used
     *
     *
     * @return The unclassified instances from commandline input
     */
    private Instances InstancetoInstance() {
        if (cmdline.hasOption(RACE) && cmdline.hasOption(GENDER)  && cmdline.hasOption(AGE) &&
                cmdline.hasOption(ADMISSION_TYPE_ID) && cmdline.hasOption(DISCHARGE_DISPOSITION_ID) &&
                cmdline.hasOption(ADMISSION_SOURCE_ID) && cmdline.hasOption(TIME_IN_HOSPITAL) &&
                cmdline.hasOption(NUM_LAB_PROCEDURES) && cmdline.hasOption(NUM_PROCEDURES) &&
                cmdline.hasOption(NUM_MEDICATION) && cmdline.hasOption(NUMBER_OUTPATIENT) &&
                cmdline.hasOption(NUMBER_EMERGENCY) && cmdline.hasOption(NUMBER_INPATIENT) && cmdline.hasOption(DIAG_1)
                && cmdline.hasOption(DIAG_2) && cmdline.hasOption(DIAG_3) && cmdline.hasOption(NUMBER_DIAGNOSES)
                && cmdline.hasOption(INSULIN) && cmdline.hasOption(CHANGE) &&
                cmdline.hasOption(DIABETESMED) && cmdline.hasOption(READMITTED)) {

            String race = cmdline.getOptionValue(RACE);
            String gender = cmdline.getOptionValue(GENDER);
            String age = cmdline.getOptionValue(AGE);
            String admission_type_id = cmdline.getOptionValue(ADMISSION_TYPE_ID);
            String discharge_disposition_id = cmdline.getOptionValue(DISCHARGE_DISPOSITION_ID);
            String admission_source_id = cmdline.getOptionValue(ADMISSION_SOURCE_ID);
            String time_in_hospital = cmdline.getOptionValue(TIME_IN_HOSPITAL);
            String num_lab_procedures = cmdline.getOptionValue(NUM_LAB_PROCEDURES);
            String num_procedures = cmdline.getOptionValue(NUM_PROCEDURES);
            String num_medications = cmdline.getOptionValue(NUM_MEDICATION);
            String number_outpatient = cmdline.getOptionValue(NUMBER_OUTPATIENT);
            String number_emergency = cmdline.getOptionValue(NUMBER_EMERGENCY);
            String number_inpatient = cmdline.getOptionValue(NUMBER_INPATIENT);
            String diag_1 = cmdline.getOptionValue(DIAG_1);
            String diag_2 = cmdline.getOptionValue(DIAG_2);
            String diag_3 = cmdline.getOptionValue(DIAG_3);
            String number_diagnoses = cmdline.getOptionValue(NUMBER_DIAGNOSES);
            String insulin = cmdline.getOptionValue(INSULIN);
            String change = cmdline.getOptionValue(CHANGE);
            String diabetesmed = cmdline.getOptionValue(DIABETESMED);
            String readmitted = cmdline.getOptionValue(READMITTED);

            ArrayList<Attribute> attributes = new ArrayList<>(25);

            attributes.add(new Attribute("race",(ArrayList<String>)null));
            attributes.add(new Attribute("gender",(ArrayList<String>)null));
            attributes.add(new Attribute("age",(ArrayList<String>)null));
            attributes.add(new Attribute("admission_type_id",(ArrayList<String>)null));
            attributes.add(new Attribute("discharge_disposition_id",(ArrayList<String>)null));
            attributes.add(new Attribute("admission_source_id",(ArrayList<String>)null));
            attributes.add(new Attribute("time_in_hospital",(ArrayList<String>)null));
            attributes.add(new Attribute("num_lab_procedures",(ArrayList<String>)null));
            attributes.add(new Attribute("num_procedures",(ArrayList<String>)null));
            attributes.add(new Attribute("num_medications",(ArrayList<String>)null));
            attributes.add(new Attribute("number_outpatient",(ArrayList<String>)null));
            attributes.add(new Attribute("number_emergency",(ArrayList<String>)null));
            attributes.add(new Attribute("number_inpatient",(ArrayList<String>)null));
            attributes.add(new Attribute("diag_1",(ArrayList<String>)null));
            attributes.add(new Attribute("diag_2",(ArrayList<String>)null));
            attributes.add(new Attribute("diag_3",(ArrayList<String>)null));
            attributes.add(new Attribute("number_diagnoses",(ArrayList<String>)null));
            attributes.add(new Attribute("insulin",(ArrayList<String>)null));
            attributes.add(new Attribute("change",(ArrayList<String>)null));
            attributes.add(new Attribute("diabetesMed",(ArrayList<String>)null));
            attributes.add(new Attribute("readmitted",(ArrayList<String>)null));

            Instances instances = new Instances("Userinstance", attributes,0);

            double[] HA1CInstance = new double[instances.numAttributes()];
            HA1CInstance[0] = instances.attribute(0).addStringValue(race);
            HA1CInstance[1] = instances.attribute(1).addStringValue(gender);
            HA1CInstance[2] = instances.attribute(2).addStringValue(age);
            HA1CInstance[3] = instances.attribute(3).addStringValue(admission_type_id);
            HA1CInstance[4] = instances.attribute(4).addStringValue(discharge_disposition_id);
            HA1CInstance[5] = instances.attribute(5).addStringValue(admission_source_id);
            HA1CInstance[6] = instances.attribute(6).addStringValue(time_in_hospital);
            HA1CInstance[7] = instances.attribute(7).addStringValue(num_lab_procedures);
            HA1CInstance[8] = instances.attribute(8).addStringValue(num_procedures);
            HA1CInstance[9] = instances.attribute(9).addStringValue(num_medications);
            HA1CInstance[10] = instances.attribute(10).addStringValue(number_outpatient);
            HA1CInstance[11] = instances.attribute(11).addStringValue(number_emergency);
            HA1CInstance[12] = instances.attribute(12).addStringValue(number_inpatient);
            HA1CInstance[13] = instances.attribute(13).addStringValue(diag_1);
            HA1CInstance[14] = instances.attribute(14).addStringValue(diag_2);
            HA1CInstance[15] = instances.attribute(15).addStringValue(diag_3);
            HA1CInstance[16] = instances.attribute(16).addStringValue(number_diagnoses);
            HA1CInstance[17] = instances.attribute(17).addStringValue(insulin);
            HA1CInstance[18] = instances.attribute(18).addStringValue(change);
            HA1CInstance[19] = instances.attribute(19).addStringValue(diabetesmed);
            HA1CInstance[20] = instances.attribute(20).addStringValue(readmitted);

            instances.add(new DenseInstance(1.0, HA1CInstance));

            instances = setClass(instances);
            return instances; }
        else {
            System.err.println("Some required fields are missing arguments," +
                    " please check --help and fill in all the missing fields!");
            System.exit(0);
            return  null;
        }
    }

    /**
     * Create attribute HBA1Cresult to predict with 2 options, to high or normal
     *
     * @return The Instances object with class attribute added
     */
    private Instances setClass(Instances instances){
        Instances unclassified = new Instances(instances);
        //System.out.println(unclassified);
        Add filter = new Add();
        filter.setAttributeIndex("last");
        filter.setNominalLabels("Too high, Normal");
        filter.setAttributeName("A1Cresult");
        try {
            filter.setInputFormat(unclassified);
            unclassified = Filter.useFilter(unclassified, filter);

            unclassified.setClassIndex(unclassified.numAttributes() - 1);
            System.out.println(unclassified);
            return unclassified;
        } catch (Exception e) {
            System.err.println("HBA1C level could not be applied");
            System.exit(0);
        }
        return null;
    }



    /**
     * Instances to the ClassifyInstances class and classifie.
     *
     * @return The classified instances
     */
    private Instances classifyInstances(Instances unclassifiedInstances) {
        ClassifyInstances classifier = new ClassifyInstances(unclassifiedInstances);
        return classifier.classify();
    }


    /**
     * Print the classified instances
     */
    private void printInstances(Instances classifiedInstances){
        System.out.println(classifiedInstances);
        System.exit(0);
    }
}




